import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import '../assets/css/home.css';

export default function Home() {
    return(
        <Container className="mt-5">
            <h1 className="text-center mb-4">Qui sommes-nous ?</h1>
            <Row>
                <Col md={6}>
                    <h2>Notre succès</h2>
                    <p>
                        Le succès de Mercadona repose sur plusieurs piliers, notamment des produits de qualité à des prix compétitifs et un excellent service client. Mais ce qui distingue réellement l'entreprise de ses concurrents, c'est sa politique de promotions fréquentes. En effet, il y a toujours plus de 100 produits en promotion à tout moment de l'année, offrant ainsi aux clients des opportunités d'économies considérables.
                    </p>
                    <br/>
                    <h2>Notre engagement environnemental</h2>
                    <p>
                        Chez Mercadona, nous prenons notre engagement environnemental très au sérieux. Nous sommes conscients que les entreprises doivent jouer leur rôle dans la préservation de notre planète. C'est pourquoi nous avons entrepris des actions pour réduire notre impact environnemental. Nous avons par exemple adopté une politique de recyclage et nous travaillons actuellement sur la réduction de notre consommation d'énergie.
                    </p>
                </Col>
                <Col md={6}>
                    <h2>Notre projet écologique</h2>
                    <p>
                        Cependant, nous savons que nous pouvons faire plus pour l'environnement. C'est pourquoi nous avons décidé de remplacer nos tracts publicitaires par une application web respectueuse de l'environnement. Nous sommes conscients que nous ne sommes pas des experts dans le domaine du développement web, c'est pourquoi nous avons fait appel à PromoWeb, une entreprise spécialisée dans la création d'applications web.
                    </p>
                    <br/>
                    <h2>Notre collaboration avec PromoWeb</h2>
                    <p>
                        Notre collaboration avec PromoWeb se déroule très bien. Nous avons nommé José, un membre de notre personnel, pour être le représentant de Mercadona pour ce projet. PromoWeb nous a fourni des conseils experts et nous a aidé à créer une application web qui sera non seulement écologique, mais aussi facile à utiliser pour nos clients.
                    </p>
                </Col>
            </Row>
            <Row>
                <Col md={12}>
                    <br/>
                    <h2>Notre engagement envers nos clients</h2>
                    <p>Chez Mercadona, notre engagement envers nos clients est au cœur de notre entreprise. Nous sommes fiers d'offrir des produits de qualité à des prix compétitifs, ainsi qu'un service clientèle exceptionnel. Nous sommes déterminés à continuer à répondre aux besoins de nos clients, tout en respectant notre engagement envers l'environnement et en apportant des améliorations constantes.</p>
                </Col>
            </Row>
        </Container>
    );
}
