import React, {useContext, useEffect, useState} from "react";
import { Link, useNavigate } from "react-router-dom";
import {Dropdown, Nav, Navbar, NavDropdown} from "react-bootstrap";
import logo from "../assets/image/logo.png"
import JsonContext from "../context/JsonContext";
import Cookies from "js-cookie";

// @ts-ignore
export default function PageNavbar() {

    let navigate = useNavigate()

    // @ts-ignore
    const { token, setToken } = useContext(JsonContext);

    function logout() {
        Cookies.remove("TOKEN");
        console.log("Cookies.remove(\"TOKEN\") : ", Cookies.remove("TOKEN"))
        setToken(undefined);
        console.log("setToken : ", setToken)
        navigate("/");
    }

    return (

        <Navbar className="nav" expand="lg" bg="dark" variant="dark">
            <Navbar.Brand href="#home">
                <img src={logo} alt='logo' className="logo" />
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />

                <Navbar.Collapse id="responsive-navbar-nav">
                    <Link to="/" className="nav-link">
                        <span>Accueil</span>
                    </Link>
                    <Link to="/catalogue" className="nav-link">
                        <span>Catalogue</span>
                    </Link>
                    {token ? (
                        <>
                            <Link to="/ajoutProduit" className="nav-link">
                                <span>Ajouter un produit</span>
                            </Link>
                            <Link to="/ajoutCategorie" className="nav-link">
                                <span>Ajouter une catégorie</span>
                            </Link>
                            <Link to="/" className="nav-link" onClick={logout}>
                                <span>Déconnexion</span>
                            </Link>
                        </>
            ) : (
                    <Link to="/login" className="nav-link">
                        <span>Connexion</span>
                    </Link>
            )}
                </Navbar.Collapse>

        </Navbar>
    );
}