import React, {useEffect, useState} from "react";
import Cookies from 'js-cookie';
import JsonContext from "./JsonContext";
import jwtDecode from "jwt-decode";

interface DecodedToken {
    user_id: string;
    roles: string[];
    // add other properties as needed
}


export default function TokenContextProvider(props: { children: string | number | boolean | React.ReactElement<any, string | React.JSXElementConstructor<any>> | React.ReactFragment | React.ReactPortal | null | undefined; }) {
    /*    const [roles, setRoles] = useState<string[]>([]);
        const [token, setToken] = useState<string>(() => {
            const cookieToken = Cookies.get("TOKEN");
            if (cookieToken && cookieToken !== "") {
                return JSON.parse(cookieToken);
            }
          return "";
        });

        useEffect(() => {
            const token = Cookies.get("TOKEN");
            if (token) {
                const decodedToken = jwtDecode(token) as DecodedToken;
                setRoles(decodedToken.roles);
            }
        }, []);

        const handleSetToken = (newToken: string) => {
            setToken(newToken);
            const decodedToken = jwtDecode(newToken) as DecodedToken;
            setRoles(decodedToken.roles);
            Cookies.set("TOKEN", newToken);
        };

        const decodedToken = jwtDecode(token) as DecodedToken;
        const userId = decodedToken.user_id;

     */

    const [token, setToken] = useState<string | undefined>(() => {
        const token = Cookies.get('TOKEN');
        if (token) {
            return JSON.parse(token);
        }
        return undefined;
    });

    const [userId, setUserId] = useState<string>();

    const handleSetToken = (token: string) => {
        setToken(token);
        console.log('token : ', token)
        Cookies.set('TOKEN', token);
    };

    return (
        // @ts-ignore
        // <JsonContext.Provider value={{ token, userId, roles, setToken: handleSetToken }}>
        <JsonContext.Provider value={{ token, setToken: handleSetToken }}>
            {props.children}
        </JsonContext.Provider>
    );
}