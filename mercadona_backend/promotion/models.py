from django.contrib.auth.models import User
from django.db import models

from product.models import Product


# Create your models here.
class Promotion(models.Model):
    date_of_begin = models.DateField()
    date_of_end = models.DateField()
    percentage = models.FloatField()
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name="promotions")
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    class Meta:
        verbose_name = 'Promotion'
        verbose_name_plural = 'Promotions'