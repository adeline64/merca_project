from rest_framework import serializers

from promotion.models import Promotion


class PromotionSerializer(serializers.ModelSerializer):

    dateDeDebut = serializers.DateField(source='date_of_bigin')
    dateDeFin = serializers.DateField(source='date_of_end')
    pourcentage = serializers.FloatField(source='percentage')

    class Meta:
        model = Promotion
        exclude = ['date_of_begin', 'date_of_end', 'percentage']