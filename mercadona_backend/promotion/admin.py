from django.contrib import admin

from promotion.models import Promotion


# Register your models here.
@admin.register(Promotion)
class PromotionAdmin(admin.ModelAdmin):

    def product_name(self, obj):
        return obj.product.wording

    product_name.admin_order_field = 'product__wording'

    list_display = ('date_of_begin', 'date_of_end', 'percentage', 'product_name',)
