from rest_framework import serializers

from product.models import Product


class ProductSerializer(serializers.ModelSerializer):

    libelle = serializers.CharField(source='wording')
    prix = serializers.FloatField(source='price')
    image = serializers.FileField(source='picture')

    class Meta:
        model = Product
        exclude = ['wording', 'price', 'picture']